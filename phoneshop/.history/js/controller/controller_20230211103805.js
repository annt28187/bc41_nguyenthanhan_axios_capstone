let contentItem = (phone) => {
  return ` <div class="col-10 mx-auto col-sm-6 col-md-4 col-lg-3 mt-4">
<div class="card">
    <img src="${phone.image}" class="card-img-top" alt="...">
    <div class="card-body">
        <h5 class="card-title">${phone.name}</h5>
       <h4 class="text-warning">${phone.price}$</h4>
        <section class="mt-2">
            <button onclick="like('${phone.id}')" type="button" class="btn btn-outline-primary"><i id="phone_${phone.id}" class="fa-solid fa-heart"></i></button>
            <button onclick="shopIncrease('${phone.id}')" type="button" class="btn btn-outline-warning"><i class="fa-solid fa-cart-shopping"></i></button>
           
        </section>
    </div>
    <div class="showDetail p-3">
    <li><i class="fa-solid fa-caret-right"></i> Màn hình:${phone.screen}</li>
    <li><i class="fa-solid fa-caret-right"></i> Camera sau: ${phone.backCamera}</li>
    <li><i class="fa-solid fa-caret-right"></i> Camera trước: ${phone.frontCamera}</li>
    <li><i class="fa-solid fa-caret-right"></i> ${phone.describe}</li>
    </div>
</div>
</div>
`;
};

export let renderPhone = (phoneArr) => {
  let contentHTML = '';
  phoneArr.forEach((phone) => {
    let content = contentItem(phone);
    contentHTML += content;
  });
  getID('show').innerHTML = contentHTML;
  likeColor(phoneArr);
};

export let search = (phoneArr) => {
  let typePhone = getID('search').value;
  let contentHTML = '';
  phoneArr.forEach((phone) => {
    if (phone.type === typePhone) {
      let content = contentItem(phone);
      contentHTML += content;
    }
  });
  getID('show').innerHTML = contentHTML;
  likeColor(phoneArr);
};

export let searchMy = (phoneArr) => {
  let contentHTML = '';
  phoneArr.forEach((phone) => {
    if (phone.like) {
      let content = contentRepeat(phone);
      contentHTML += content;
    }
  });
  getID('show').innerHTML = contentHTML;
  likeColor(phoneArr);
};

let likeColor = (phoneArr) => {
  phoneArr.forEach((item) => {
    if (item.like) {
      let a = getID('phone_' + item.id);
      if (a != null) {
        a.style.color = 'red';
      }
    }
  });
};

export let renderBuy = (list) => {
  let total = 0;
  let totalNumber = 0;
  let contentHTML = '';
  list.forEach((item) => {
    if (item.quantity != 0) {
      total += item.price * item.quantity;
      totalNumber += item.quantity;
      let content = `
<tr>
<td>
    <img src="${item.img}"
        alt="">
    <h5>${item.name}</h5>
</td>
<td><h5>${item.price * item.quantity} $</h5></td>
<td><button onclick="shopDecrease('${
        item.id
      }')" type="button" class="btn btn-warning">-</button><span class="mx-2">${
        item.quantity
      }</span><button onclick="shopIncrease('${
        item.id
      }')" type="button" class="btn btn-primary">+</button></td><td><i class="fas fa-trash" onclick="shopDelete('${
        item.id
      }')"></i></td>
</tr>
`;
      contentHTML += content;
    }
  });
  let addTotal = `  <tr>
    <td></td>
    <td>
        <h1 id="checkTotal">${total}$</h1>
    </td>
    <td><button onclick="clearShop()" type="button" class="btn btn-success m-1">Buy now</button><button onclick="closeBag()" type="button" class="btn btn-danger m-1">Close</button></td>
</tr>
    `;
  document.getElementById('showTable').innerHTML = contentHTML + addTotal;
  if (totalNumber != 0) {
    document.getElementById('itemNumer').innerHTML = totalNumber;
  } else {
    document.getElementById('itemNumer').innerHTML = '';
  }
};

export let onOffLoading = () => {
  let change = document.getElementById('onOff');
  if (change.style.display == 'none') {
    change.style.display = 'block';
  } else {
    change.style.display = 'none';
  }
};

export let getID = (name) => {
  return document.getElementById(name);
};
