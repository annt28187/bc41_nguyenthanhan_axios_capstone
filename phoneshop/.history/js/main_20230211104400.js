import {
  getID,
  onOffLoading,
  renderBuy,
  renderPhone,
  search,
  searchMy,
} from './controller/controller.js';
const BASE_URL = 'https://63bea7fce348cb0762149aa0.mockapi.io/phone';
let productList = [];
let buyList = [];
const buyList_LOCAL = 'buyList_LOCAL';
let buyListJSON = localStorage.getItem(buyList_LOCAL);

let getData = () => {
  axios({
    url: BASE_URL,
    method: 'GET',
  })
    .then(function (res) {
      console.log(res.data);
      productList = res.data.map((item) => (item = { ...item, like: false }));

      if (buyListJSON != null) {
        buyList = JSON.parse(buyListJSON);
        buyList.forEach((item, i) => {
          let index = productList.findIndex((phone) => phone.id == item.id);
          if (index == -1) {
            buyList.splice(i, 1);
          }
        });
        buyListJSON = JSON.stringify(buyList);
        localStorage.setItem(buyList_LOCAL, buyListJSON);

        renderBuy(buyList);
      }
      onOffLoading();
      renderPhone(productList);
    })
    .catch(function (err) {
      alert('kiểm tra kêt nối mạng');
      console.log(err);
    });
};
getData();

getID('search').addEventListener('change', () => {
  let getSearch = getID('search').value;
  if (getSearch == 'all') {
    renderPhone(productList);
  } else if (getSearch == 'my') {
    searchMy(productList);
  } else {
    search(productList);
  }
});

let like = (phone) => {
  let a = getID('phone_' + phone);
  productList.forEach((item, i) => {
    if (item.id == phone) {
      if (productList[i].like) {
        productList[i].like = false;
      } else {
        productList[i].like = true;
      }
    }
  });

  if (a.style.color == '') {
    a.style.color = 'red';
  } else {
    a.style.color = '';
  }
  let getSearch = getID('search').value;
  if (getSearch == 'my') {
    searchMy(productList);
  }
};
window.like = like;

let shopIncrease = (phone) => {
  let index = buyList.findIndex((item) => item.id == phone);
  if (index == -1) {
    let index = productList.findIndex((item) => item.id == phone);
    let newPhone = { ...productList[index], quantity: 1 };
    buyList.push(newPhone);
  } else {
    buyList[index].quantity++;
  }

  buyListJSON = JSON.stringify(buyList);
  localStorage.setItem(buyList_LOCAL, buyListJSON);

  renderBuy(buyList);
};
function shopDecrease(phone) {
  let index = buyList.findIndex((item) => item.id == phone);
  buyList[index].quantity--;

  buyListJSON = JSON.stringify(buyList);
  localStorage.setItem(buyList_LOCAL, buyListJSON);
  renderBuy(buyList);
}
window.shopDecrease = shopDecrease;
document.getElementById('ShowShop').addEventListener('click', () => {
  let show = document.getElementById('showBag');
  if (show.style.display == 'block') {
    show.style.display = 'none';
  } else {
    show.style.display = 'block';
  }

  renderBuy(buyList);
});
window.shopIncrease = shopIncrease;
function shopDelete(phone) {
  let index = buyList.findIndex((item) => item.id == phone);
  buyList.splice(index, 1);

  buyListJSON = JSON.stringify(buyList);
  localStorage.setItem(buyList_LOCAL, buyListJSON);
  renderBuy(buyList);
}
window.shopDelete = shopDelete;
function clearShop() {
  if (buyList.length == 0) {
    alert('Bạn chưa chọn sản phẩm');
    return;
  }

  buyList.splice(0);

  document.getElementById('showBag').style.display = 'none';

  buyListJSON = JSON.stringify(buyList);
  localStorage.setItem(buyList_LOCAL, buyListJSON);
  alert('Chúc mừng bạn đã đặt hàng thành công');
  renderBuy(buyList);
}
window.clearShop = clearShop;
function closeBag() {
  document.getElementById('showBag').style.display = 'none';
}
window.closeBag = closeBag;
