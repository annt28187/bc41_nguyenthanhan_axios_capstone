import { showMessageError, getID } from './phoneController.js';

let checkEmpty = (value, idError, message) => {
  if (value == '' || value.selectedIndex == 0) {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  } else {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  }
};

let checkDescription = (value, maxLength, idError) => {
  let length = value.length;
  if (length > maxLength) {
    getID(idError).innerText = `Không vượt quá ${maxLength} ký tự`;
    getID(idError).style.display = 'block';
    return false;
  } else {
    getID(idError).innerText = '';
    getID(idError).style.display = 'none';
    return true;
  }
};

export let validation = (phone) => {
  let isValid = true;

  // Kiểm tra rỗng
  isValid =
    checkEmpty(phone.name, 'tbName', 'Tên SP không để trống') &
    checkEmpty(phone.price, 'tbPrice', 'Giá SP không để trống') &
    checkEmpty(phone.screen, 'tbScreen', 'Màn hình SP không để trống') &
    checkEmpty(phone.backCamera, 'tbBackCamera', 'Camera sau của SP không để trống') &
    checkEmpty(phone.frontCamera, 'tbFrontCamera', 'Camera trước của SP không để trống') &
    checkEmpty(phone.type, 'tbType', 'Kiểu của SP không để trống') &
    checkEmpty(phone.image, 'tbLink', 'Link của SP không để trống') &
    checkEmpty(phone.describe, 'tbDesc', 'Mô tả SP không để trống');

  if (isValid) {
    isValid = checkDescription(phone.describe, 60, 'tbDesc');
  }
  return isValid;
};
