let phoneItem = (item, i) => {
  let contentTR = `<tr>
                    <td>${i + 1}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td><img style="width:80px;" src="${item.image}" alt="${item.name}"></td>
                    <td>
                        <span>${item.frontCamera},${item.backCamera},${item.screen}</span><br/>
                        <span>${item.describe}</span><br/>
                        <span>${item.type}</span>
                    </td>
                    <td> <button onclick="deletePhone(${
                      item.id
                    })" class="btn btn-danger">Xoá</button> 
                        <button onclick="editPhone(${item.id})"class="btn btn-warning">Sửa</button>
                      </td>
                </tr>`;
  return contentTR;
};

export let renderListPhone = (phoneArr) => {
  let nameSearch = document.getElementById('searchPhone').value;
  let contentHTML = '';
  if (nameSearch == '') {
    phoneArr.forEach((phone, index) => {
      let i = phoneItem(phone, index);
      contentHTML += i;
    });
  } else {
    let newPhone = [];
    phoneArr.forEach((item) => {
      if (item.type == nameSearch) {
        newPhone.push(item);
      }
    });
    phoneArr.forEach((phone, index) => {
      let i = phoneItem(phone, index);
      contentHTML += i;
    });
  }

  document.getElementById('tbodyData').innerHTML = contentHTML;
};

export let getInfo = () => {
  let name = document.getElementById('phoneName').value;
  let price = document.getElementById('phonePrice').value;
  let frontCamera = document.getElementById('phoneFrontCamera').value;
  let backCamera = document.getElementById('phoneBackCamera').value;
  let screen = document.getElementById('phoneScreen').value;
  let type = document.getElementById('phoneType').value;
  let img = document.getElementById('phoneLink').value;
  let desc = document.getElementById('phoneDesc').value;
  if (
    name == '' ||
    price == '' ||
    frontCamera == '' ||
    backCamera == '' ||
    screen == '' ||
    type == '' ||
    img == '' ||
    desc == ''
  ) {
    alert('Dữ liệu không được để trống, xin bạn kiểm tra!');
    return -1;
  }
  return new Phone(name, price, screen, backCamera, frontCamera, img, desc, type);
};
export let resetInput = () => {
  document.getElementById('phoneName').value = '';
  document.getElementById('phonePrice').value = '';
  document.getElementById('phoneCamera').value = '';
  document.getElementById('phoneBackCamera').value = '';
  document.getElementById('phoneScreen').value = '';
  document.getElementById('phoneType').value = '';
  document.getElementById('phoneLink').value = '';
  document.getElementById('phoneDesc').value = '';
};
