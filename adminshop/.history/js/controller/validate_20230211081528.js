import { showMessageError } from './phoneController.js';

let checkEmpty = (value, idError, message) => {
  if (value == '' || value.selectedIndex == 0) {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  } else {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  }
};

let checkSelection = (value, idError, message) => {
  let tagSelect = getID(value);
  if (tagSelect.selectedIndex == 0) {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  } else {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  }
};

let checkDescription = (value, maxLength, idError) => {
  let length = value.length;
  if (length > maxLength) {
    getID(idError).innerText = `Không vượt quá ${maxLength} ký tự`;
    getID(idError).style.display = 'block';
    return false;
  } else {
    getID(idError).innerText = '';
    getID(idError).style.display = 'none';
    return true;
  }
};

export let validationAdd = (phone) => {
  let isValid = true;

  // Kiểm tra rỗng 
  isValid = checkEmpty(phone.)

};

function validationAdd(user) {
  // Tài khoản ND
  var isValidTK = true;
  isValidTK =
    kiemTraRong(user.taiKhoan, 'tbTaiKhoan', 'Tài khoản ND không để trống') &&
    kiemTraTrung(user.taiKhoan, ndArr);

  // Họ tên ND
  var isValidHT = true;
  isValidHT =
    kiemTraRong(user.hoTen, 'tbHoTen', 'Họ tên ND không để trống') &&
    kiemTraChu(user.hoTen, 'tbHoTen', 'Họ tên ND phải là không chứa ký tự đặt biệt');

  // Pass ND
  var isValidPass = true;
  isValidPass =
    kiemTraRong(user.matKhau, 'tbMatKhau', 'Mật khẩu ND không để trống') &&
    kiemTraMatKhau(user.matKhau);

  // Email
  var isValidEmail = true;
  isValidEmail =
    kiemTraRong(user.email, 'tbEmail', 'Email ND không để trống') && kiemTraEmail(user.email);

  // Hình ảnh
  var isValidImage = true;
  isValidImage = kiemTraRong(user.hinhAnh, 'tbHinhAnh', 'Hình ảnh ND không để trống');

  // Loại người dùng
  var isValidLN = true;
  isValidLN = kiemTraLuaChon('loaiNguoiDung', 'tbLoaiNguoiDung', 'Chưa chọn loại ND');

  // Loại ngôn ngữ
  var isValidNN = true;
  isValidNN = kiemTraLuaChon('loaiNgonNgu', 'tbLoaiNgonNgu', 'Chưa chọn loại ngôn ngữ');

  // Mô tả
  var isValidMT = true;
  isValidMT =
    kiemTraRong(user.moTa, 'tbMoTa', 'Mô tả ND không để trống') &&
    kiemTraMoTa(user.moTa, 60, 'tbMoTa');

  var isValid =
    isValidTK &
    isValidHT &
    isValidPass &
    isValidEmail &
    isValidImage &
    isValidLN &
    isValidNN &
    isValidMT;
  return isValid;
}

function validationUpdate(user, account) {
  // Tài khoản ND

  var isCheck = true;
  if (user.taiKhoan != account) isCheck = kiemTraTrung(user.taiKhoan, ndArr);

  var isValidTK = true;
  isValidTK = kiemTraRong(user.taiKhoan, 'tbTaiKhoan', 'Tài khoản ND không để trống') && isCheck;

  // Họ tên ND
  var isValidHT = true;
  isValidHT =
    kiemTraRong(user.hoTen, 'tbHoTen', 'Họ tên ND không để trống') &&
    kiemTraChu(user.hoTen, 'tbHoTen', 'Họ tên ND phải là không chứa ký tự đặt biệt');

  // Pass ND
  var isValidPass = true;
  isValidPass =
    kiemTraRong(user.matKhau, 'tbMatKhau', 'Mật khẩu ND không để trống') &&
    kiemTraMatKhau(user.matKhau);

  // Email
  var isValidEmail = true;
  isValidEmail =
    kiemTraRong(user.email, 'tbEmail', 'Email ND không để trống') && kiemTraEmail(user.email);

  // Hình ảnh
  var isValidImage = true;
  isValidImage = kiemTraRong(user.hinhAnh, 'tbHinhAnh', 'Hình ảnh ND không để trống');

  // Loại người dùng
  var isValidLN = true;
  isValidLN = kiemTraLuaChon('loaiNguoiDung', 'tbLoaiNguoiDung', 'Chưa chọn loại ND');

  // Loại ngôn ngữ
  var isValidNN = true;
  isValidNN = kiemTraLuaChon('loaiNgonNgu', 'tbLoaiNgonNgu', 'Chưa chọn loại ngôn ngữ');

  // Mô tả
  var isValidMT = true;
  isValidMT =
    kiemTraRong(user.moTa, 'tbMoTa', 'Mô tả ND không để trống') &&
    kiemTraMoTa(user.moTa, 60, 'tbMoTa');

  var isValid =
    isValidTK &
    isValidHT &
    isValidPass &
    isValidEmail &
    isValidImage &
    isValidLN &
    isValidNN &
    isValidMT;
  return isValid;
}
