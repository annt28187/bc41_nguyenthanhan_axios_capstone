import { Phone } from '../model/phoneModel.js';

let phoneItem = (item, i) => {
  let contentTR = `<tr>
                    <td>${i + 1}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td><img style="width:80px;" src="${item.image}" alt="${item.name}"></td>
                    <td>
                        <span>${item.frontCamera},${item.backCamera},${item.screen}</span><br/>
                        <span>${item.describe}</span><br/>
                        <span>${item.type}</span>
                    </td>
                    <td> <button onclick="deletePhone(${
                      item.id
                    })" class="btn btn-danger"><i class="fa-solid fa-trash-can"></i></button> 
                        <button onclick="editPhone(${
                          item.id
                        })"class="btn btn-warning"><i class="fa fa-edit"></i></button>
                      </td>
                </tr>`;
  return contentTR;
};

export let renderListPhone = (phoneArr) => {
  let nameSearch = document.getElementById('searchPhone').value;
  let contentHTML = '';
  if (nameSearch == '') {
    phoneArr.forEach((phone, index) => {
      let i = phoneItem(phone, index);
      contentHTML += i;
    });
  } else {
    let newPhone = [];
    phoneArr.forEach((item) => {
      if (item.type == nameSearch) {
        newPhone.push(item);
      }
    });
    phoneArr.forEach((phone, index) => {
      let i = phoneItem(phone, index);
      contentHTML += i;
    });
  }

  getID('tbodyData').innerHTML = contentHTML;
};

export let getInfoPhone = () => {
  let name = getID('phoneName').value;
  let price = getID('phonePrice').value;
  let screen = getID('phoneScreen').value;
  let backCamera = getID('phoneBackCamera').value;
  let frontCamera = getID('phoneFrontCamera').value;
  let image = getID('phoneLink').value;
  let describe = getID('phoneDesc').value;
  let type = getID('phoneType').value;
  let phone = new Phone(name, price, screen, backCamera, frontCamera, image, describe, type);
  return phone;
};

export let resetInput = () => {
  getID('phoneName').value = '';
  getID('phonePrice').value = '';
  getID('phoneScreen').value = '';
  getID('phoneBackCamera').value = '';
  getID('phoneFrontCamera').value = '';
  getID('phoneLink').value = '';
  getID('phoneDesc').value = '';
  getID('phoneType').value = '';
};

export let getID = (name) => {
  return document.getElementById(name);
};

export let onSuccess = (name) => {
  Toastify({
    text: `${name} thành công`,
    duration: 3000,
  }).showToast();
};

export let onFail = (name) => {
  Toastify({
    text: `${name} thất bại`,
    duration: 3000,
  }).showToast();
};

export let enableLoading = () => {
  getID('loading').style.display = 'flex';
};

export let disableLoading = () => {
  getID('loading').style.display = 'none';
};
