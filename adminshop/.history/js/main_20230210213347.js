import { renderListPhone, onSuccess, onFail } from './controller/phoneController.js';

const BASE_URL = 'https://63bea7fce348cb0762149aa0.mockapi.io/phone';
let id = '';
let phoneArr = [];

let getPhoneList = () => {
  axios({
    url: BASE_URL,
    method: 'GET',
  })
    .then((res) => {
      phoneArr = [...res.data];
      renderListPhone(phoneArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

getPhoneList();

let deletePhone = (id) => {
  let name = 'Xóa';
  axios({
    url: `${BASE_URL}/${id}`,
    method: 'DELETE',
  })
    .then((res) => {
      onSuccess(name);
      console.log('delete', res.data);
      getPhoneList();
    })
    .catch((err) => {
      onFail(name);
      console.log(err);
    });
};

window.deletePhone = deletePhone;
