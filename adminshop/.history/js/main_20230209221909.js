import { renderListPhone } from './controller/phoneController.js';

const BASE_URL = 'https://63bea7fce348cb0762149aa0.mockapi.io/phone';
let id = '';
let phoneArr = [];

let getPhoneList = () => {
  axios({
    url: BASE_URL,
    method: 'GET',
  })
    .then((res) => {
      phoneArr = [...res.data];
      renderListPhone(phoneArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

getPhoneList();
