import {
  renderListPhone,
  resetInput,
  getInfoPhone,
  onSuccess,
  onFail,
  getID,
  enableLoading,
  disableLoading,
} from './controller/phoneController.js';
import { validation } from './controller/validate.js';

const BASE_URL = 'https://63bea7fce348cb0762149aa0.mockapi.io/phone';
let idFind = 0;
let phoneArr = [];

let getPhoneList = () => {
  enableLoading();
  axios({
    url: BASE_URL,
    method: 'GET',
  })
    .then((res) => {
      disableLoading();
      phoneArr = [...res.data];
      renderListPhone(phoneArr);
    })
    .catch((err) => {
      disableLoading();
      console.log(err);
    });
};

getPhoneList();

let deletePhone = (id) => {
  let name = 'Xóa sản phẩm';
  axios({
    url: `${BASE_URL}/${id}`,
    method: 'DELETE',
  })
    .then((res) => {
      onSuccess(name);
      console.log('delete', res.data);
      getPhoneList();
    })
    .catch((err) => {
      onFail(name);
      console.log(err);
    });
};

window.deletePhone = deletePhone;

let editPhone = (id) => {
  getID('updatePhone').style.display = 'block';
  getID('addPhone').style.display = 'none';
  let index = phoneArr.findIndex((item) => {
    return item.id == id;
  });
  idFind = id;
  getID('phoneName').value = phoneArr[index].name;
  getID('phonePrice').value = phoneArr[index].price;
  getID('phoneScreen').value = phoneArr[index].screen;
  getID('phoneBackCamera').value = phoneArr[index].backCamera;
  getID('phoneFrontCamera').value = phoneArr[index].frontCamera;
  getID('phoneLink').value = phoneArr[index].img;
  getID('phoneDesc').value = phoneArr[index].desc;
  getID('phoneType').value = phoneArr[index].type;
  $('#exampleModal').modal('show');
};

window.editPhone = editPhone;

getID('updatePhone').addEventListener('click', () => {
  let phoneUpdate = getInfoPhone();
  let name = 'Cập nhật sản phẩm';
  if (phoneUpdate == -1) return;
  enableLoading;
  axios({
    url: `${BASE_URL}/${idFind}`,
    method: 'PUT',
    data: phoneUpdate,
  })
    .then((res) => {
      disableLoading();
      console.log('Update', res.data);
      onSuccess(name);
      $('#exampleModal').modal('hide');
      getPhoneList();
      getID('addPhone').style.display = 'block';
    })
    .catch((err) => {
      disableLoading();
      onFail(name);
      console.log(err);
    });
});

getID('addPhone').addEventListener('click', () => {
  let phoneAdd = getInfoPhone();
  let name = 'Thêm sản phẩm';
  if (phoneAdd == -1) return;
  enableLoading;
  let isValid = validation(phoneAdd);
  if (isValid) {
    axios({
      url: BASE_URL,
      method: 'POST',
      data: phoneAdd,
    })
      .then((res) => {
        disableLoading();
        console.log('Add', res.data);
        onSuccess(name);
        getPhoneList();
        resetInput();
      })
      .catch((err) => {
        disableLoading();
        onFail(name);
        console.log(err);
      });
  } else {
    disableLoading();
  }
});

getID('modalButton').addEventListener('click', () => {
  getID('updatePhone').style.display = 'none';
  getID('addPhone').style.display = 'block';
  resetInput();
});

getID('searchPhone').addEventListener('change', () => {
  renderListPhone(phoneArr);
});
