export class Phone {
  constructor(name, price, screen, backCamera, frontCamera, image, describe, type) {
    this.name = name;
    this.price = price;
    this.screen = screen;
    this.backCamera = backCamera;
    this.frontCamera = frontCamera;
    this.image = image;
    this.describe = describe;
    this.type = type;
  }
}
