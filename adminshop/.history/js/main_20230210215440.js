import { renderListPhone, onSuccess, onFail } from './controller/phoneController.js';

const BASE_URL = 'https://63bea7fce348cb0762149aa0.mockapi.io/phone';
let id = '';
let phoneArr = [];

let getPhoneList = () => {
  axios({
    url: BASE_URL,
    method: 'GET',
  })
    .then((res) => {
      phoneArr = [...res.data];
      renderListPhone(phoneArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

getPhoneList();

let deletePhone = (id) => {
  let name = 'Xóa';
  axios({
    url: `${BASE_URL}/${id}`,
    method: 'DELETE',
  })
    .then((res) => {
      onSuccess(name);
      console.log('delete', res.data);
      getPhoneList();
    })
    .catch((err) => {
      onFail(name);
      console.log(err);
    });
};

window.deletePhone = deletePhone;

let editPhone = (id) => {
  document.getElementById('updatePhone').style.display = 'block';
  document.getElementById('addPhone').style.display = 'none';
  let index = phoneArr.findIndex((item) => {
    return item.id == id;
  });
  idFind = id;
  document.getElementById('phoneName').value = phoneArr[index].name;
  document.getElementById('phonePrice').value = phoneArr[index].price;
  document.getElementById('phoneScreen').value = phoneArr[index].screen;
  document.getElementById('phoneBackCamera').value = phoneArr[index].backCamera;
  document.getElementById('phoneFrontCamera').value = phoneArr[index].frontCamera;
  document.getElementById('phoneType').value = phoneArr[index].type;
  document.getElementById('phoneLink').value = phoneArr[index].img;
  document.getElementById('phoneDesc').value = phoneArr[index].desc;
};

window.editPhone = editPhone;

document.getElementById('modalButton').addEventListener('click', () => {
  document.getElementById('updatePhone').style.display = 'none';
  document.getElementById('addPhone').style.display = 'block';
  resetInput();
});

document.getElementById('searchPhone').addEventListener('change', () => {
  renderListPhone(phoneArr);
});
