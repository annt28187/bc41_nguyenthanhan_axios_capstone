import { renderListPhone, onSuccess, onFail, getID } from './controller/phoneController.js';

const BASE_URL = 'https://63bea7fce348cb0762149aa0.mockapi.io/phone';
let idFind = '';
let phoneArr = [];

let getPhoneList = () => {
  enableLoading();
  axios({
    url: BASE_URL,
    method: 'GET',
  })
    .then((res) => {
      phoneArr = [...res.data];
      renderListPhone(phoneArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

getPhoneList();

let deletePhone = (id) => {
  let name = 'Xóa';
  axios({
    url: `${BASE_URL}/${id}`,
    method: 'DELETE',
  })
    .then((res) => {
      onSuccess(name);
      console.log('delete', res.data);
      getPhoneList();
    })
    .catch((err) => {
      onFail(name);
      console.log(err);
    });
};

window.deletePhone = deletePhone;

let editPhone = (id) => {
  getID('updatePhone').style.display = 'block';
  getID('addPhone').style.display = 'none';
  let index = phoneArr.findIndex((item) => {
    return item.id == id;
  });
  idFind = id;
  getID('phoneName').value = phoneArr[index].name;
  getID('phonePrice').value = phoneArr[index].price;
  getID('phoneScreen').value = phoneArr[index].screen;
  getID('phoneBackCamera').value = phoneArr[index].backCamera;
  getID('phoneFrontCamera').value = phoneArr[index].frontCamera;
  getID('phoneLink').value = phoneArr[index].img;
  getID('phoneDesc').value = phoneArr[index].desc;
  getID('phoneType').value = phoneArr[index].type;
};

window.editPhone = editPhone;

getID('modalButton').addEventListener('click', () => {
  getID('updatePhone').style.display = 'none';
  getID('addPhone').style.display = 'block';
  resetInput();
});

getID('searchPhone').addEventListener('change', () => {
  renderListPhone(phoneArr);
});
